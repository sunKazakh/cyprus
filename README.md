# cyprus

Данное репо содержит решение задач.

##Задание №1

Решение находится в /script и представляет из себя: 
 
- cleaned.tsv - результат отчистки исходного файла  
- data.tsv - исходные данные  
- script.py - скрипт по отчистке данных  
 
Скрипт написан на python 2. Запуск скрипта:  
Помещаете скрипт и файл с битыми данными в одну директорию, выдаете права на выполнения и исполняете.
  
Работает на любой RHEL системе, так как там питон вшит в ось. Если запускать будетет на другой оси, то нужно 
ставить питон 2.x. 

UPDATE: теперь битые строки чинятся. 

##Задание №2

Решение находится в /map_reduce и представляет из себя maven проект.
Все зависимости тянутся из maven central.
Проект представляет из себя mapReduce таск с юнит тестами.
Добавил логгирование ошибок входных данных.
Чтобы запустить:  
hadoop jar exercies2-1.0-SNAPSHOT-jar-with-dependencies.jar kz.sun.kazakh.cyprus.Driver \  
 {путь в hdfs до исходных данных} {путь в hdfs для результатов}

Декларация на создании таблицы в hive:  
CREATE EXTERNAL TABLE cyprus (id int, first_name STRING, last_name STRING, account_number INT, email STRING) STORED AS ORC LOCATION 'путь до директории с результатами мапа'

Проверял запуск как mapReduce таска, так и создания таблицы в hive:  
в /map_reduce/logs - логи работы джоба  
в /map_reduce/part-m-00000 - результат мапРедьюса  
Ниже картинки работы с hive:

![Запросы](/map_reduce/hive_requests.jpg "запросы")

![Результаты](/map_reduce/results.jpg "результаты")

##Задание №3

Решение находится в /sql и представляет из себя два sql скрипта:

- postgre.sql - для запуска на постгрее.  
- t.sql - для запуска на sql server от MS.  

Оба скрипта содержат три вызова:  

1. Создание исходной таблицы  
2. Наполнение таблицы значениями  
3. Сам select. В обоих случаях использую pivot функцию  

Оба скрипта протестил:

- postgre.sql - http://rextester.com/MHW14300  
- t.sql - http://rextester.com/YLWV15969   
