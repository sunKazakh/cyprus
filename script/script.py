# coding=utf-8
import io
import re

sourcePath = 'data.tsv'
resultPath = 'cleaned.tsv'
result = io.open(resultPath, encoding='utf-8', mode='w')
number_of_skipped_lines = 0


# Метод для финального форматирования и  записи "чистой" строки
def reformat(fields):
    fields = map(unicode.strip, fields)
    result.write('\t'.join(fields))
    result.write(unicode('\n'))
    return


# Ну тут все понятно
def is_numeric(s):
    try:
        float(s)
        return True
    except (ValueError, TypeError):
        return False


# из линии, где не хватает полей строется линия с 5-ью полями. Вместо путого поля проставляется null.
def restore_line(fields):
    restoredLine = [unicode("null"), unicode("null"), unicode("null"), unicode("null"), unicode("null")]
    # Проверяю есть ли поле id
    if len(fields[0]) < 4 and is_numeric(fields[0]):
        restoredLine[0] = unicode(fields[0])
        if len(fields) > 1:
            fields = fields[1: len(fields)]
        else:
            return restoredLine
    # Проверяю есть ли поле e-mail
    if "@" in fields[len(fields) - 1]:
        restoredLine[4] = fields[len(fields) - 1]
        if len(fields) > 1:
            fields = fields[0: len(fields) - 1]
        else:
            return restoredLine
    # Проверяю есть ли поле account_number
    for field in fields:
        if len(field) and is_numeric(field[0:5]):
            restoredLine[3] = field
            fields.remove(field)
            break
    # если ещё что-то есть, то это либо имя, либо имя и фамилия
    if len(fields) == 2:
        restoredLine[1] = fields[0]
        restoredLine[2] = fields[1]
        return restoredLine
    if len(fields) == 1:
        restoredLine[1] = fields[0]
        return restoredLine
    return restoredLine


# метод с которого начинается востановление битой строки. В нем просматривается следующая, от битой, строка.
# если она тоже битая, то производится склеивание этих строк,
#  если она целая, то производится довостановление линии до целой
def find_parts_of_line(brokenLine, lines):
    number_of_skipped_lines = 1
    if len(lines[0].split('\t')) == 5 and is_numeric(lines[0].split('\t')[0]):
        reformat(restore_line(brokenLine.split('\t')))
        return number_of_skipped_lines
    else:
        fields = brokenLine.split('\t')
        for i in range(0, len(fields)):
            if fields[i].find('\n') > 0:
                fields[i] = fields[i].replace('\n', '\\n')
            if fields[i].find('\r') > 0:
                fields[i] = fields[i].replace('\r', '\\r')
            if fields[i].find('\t') > 0:
                fields[i] = fields[i].replace('\t', '\\t')
        brokenLine = '\t'.join(fields) + lines[0]
        number_of_skipped_lines += find_parts_of_line(brokenLine, lines[1: len(lines)])
        return number_of_skipped_lines


# Проверяем линию - если 5 элементов и первый число, то передаем в reformat
# Также произовдится экранирование специальных символов.
# У вас в задании очень не понятно написано про этот момент. А ещё запятых не хватает))
def transformLine(lines):
    number_of_skipped_lines = 0
    if lines[0].find('\t\t\t') > 0:
        lines[0] = lines[0].replace('\t\t\t', '\t\\t\t\\t\t')
    if lines[0].find('\t\t') > 0:
        lines[0] = lines[0].replace('\t\t', '\t\\t\t')

    fields = lines[0].split('\t')
    if len(fields) == 5 and is_numeric(fields[0]):
        reformat(fields)
    else:
        number_of_skipped_lines = find_parts_of_line(lines[0], lines[1: len(lines)])
    return int(number_of_skipped_lines)


# чтобы пропускать уже обработанные битые линии используется number_of_skipped_lines
with io.open(sourcePath, encoding='utf-16-le') as source:
    # Можно вычитать все в память, так как там всего 1000 строк
    sourceList = source.readlines(-1)
    for i in range(0, len(sourceList)):
        if number_of_skipped_lines < 2:
            if i == 0:
                print 'Headers: ' + sourceList[0]
                reformat(sourceList[0].split('\t'))
            else:
                number_of_skipped_lines = transformLine(sourceList[i: len(sourceList)])
        else:
            number_of_skipped_lines -= 1
            continue
