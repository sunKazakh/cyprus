CREATE TABLE #cyprus
    ([id] int, [name] varchar(5))
;
    
INSERT INTO #cyprus
    ([id], [name])
VALUES
    (1, 'name1'),
    (2, 'name2'),
    (3, 'name3')
;


select *
from 
(
  select id, name
  from #cyprus
) src
pivot
(
  avg(id)
  for name in ([name1], [name2], [name3])
) piv;
