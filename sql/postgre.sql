CREATE TABLE testtable(
                id      integer,
                name    text);

                   
INSERT INTO testtable VALUES
    (1, 'name1'),
    (2, 'name2'),
    (3, 'name3');
                
                
                                               
SELECT name1 , name2, name3 
FROM crosstab(
               'SELECT CAST(COUNT(id) AS text), name, CAST(id AS text)
                FROM testtable
                GROUP BY name, id
                ORDER BY 1,2'
    ) AS 
                ("row" text,  "name1" text, "name2" text, "name3" text);
