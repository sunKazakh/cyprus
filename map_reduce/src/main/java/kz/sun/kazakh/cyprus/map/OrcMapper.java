package kz.sun.kazakh.cyprus.map;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import org.apache.hadoop.hive.ql.io.orc.OrcSerde;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfo;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfoUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sbt-nazarenko-r on 20.04.2018.
 * Маппер для конвертации tsv->orc
 */
public class OrcMapper extends Mapper<LongWritable, Text, NullWritable, Writable> {
    private static final Logger LOG = LoggerFactory.getLogger(OrcMapper.class.getCanonicalName());
    private OrcSerde serde;
    private String types;
    private TypeInfo typeInfo;
    private ObjectInspector objectInspector;
    private static String delimiter = "\t";

    /**
     * Настройка маппера.
     * В основном задает типы данных для ObjectInspector
     *
     * @param context - MapContext
     */
    @Override
    protected void setup(Mapper<LongWritable, Text, NullWritable, Writable>.Context context) {
        serde = new OrcSerde();
        types = "struct<id:int,first_name:string,last_name:string,account_number:int,email:string>";
        typeInfo = TypeInfoUtils.getTypeInfoFromTypeString(types);
        objectInspector = TypeInfoUtils.getStandardJavaObjectInspectorFromTypeInfo(typeInfo);
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        if (key.get() == 0 && value.toString().contains("id")) {
            LOG.warn("Skipped line with headers");
            return;
        }
        else {
            Writable row = serde.serialize(parseLine(value), objectInspector);
            context.write(NullWritable.get(), row);
        }
    }

    /**
     * Метод для парсинга строки и приведения полей к правильным типам данных
     * Некоторые значения столбца "account_number" не типа int.
     * В задании не сказанно, что он должен быть интом,
     * но я решил сделать его int(места меньше занимает тогда, реально проверял)
     * Поэтому может выскакивать {@link NumberFormatException}.
     * Чтобы значения не терять я их привожу к инту.
     *
     * @param line - строка из tsv в типе Text
     * @return List с типизированными полями
     */
    protected static List<Object> parseLine(Text line) {
        String[] fields = cleanLine(line.toString()).split(delimiter);
        List<Object> structedFields = new LinkedList<>();
        structedFields.add(Integer.valueOf(fields[0]));
        structedFields.add(fields[1]);
        structedFields.add(fields[2]);
        try {
            structedFields.add(Integer.valueOf(fields[3]));
        }
        catch (NumberFormatException e) {
            LOG.error("Not valid value in account_number field: " + fields[3]);
            int account_number = Integer.valueOf(fields[3].replaceAll("\\D*", ""));
            structedFields.add(account_number);
            LOG.warn("Replaced " + fields[3] + " with " + account_number);
        }
        structedFields.add(fields[4]);
        return structedFields;
    }

    /**
     * Метод для очитски строки от специальных символов, которые я экранирую в первом задании.
     * Логирую ошибки во входных данных.
     *
     * @param line строка из файла
     * @return обработанная строка
     */
    protected static String cleanLine(String line) {
        if (line.contains("\\t")) {
            LOG.warn("Field: " + line + " contains special symbol \\t. The symbol will be removed");
            line = line.replace("\\t", "");
        }

        if (line.contains("\\n")) {
            LOG.warn("Field: " + line + " contains special symbol \\n. The symbol will be removed");
            line = line.replace("\\n", "");
        }
        return line;
    }

}

