package kz.sun.kazakh.cyprus;

import kz.sun.kazakh.cyprus.map.OrcMapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.orc.OrcNewOutputFormat;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sbt-nazarenko-r on 20.04.2018.
 * Драйвер для запуска MapReduce
 * На самом деле Reduce не будет, будет только Map
 * В задании было сказанно логировать все ошибки входящих данных
 * У меня ошибка появляется, если столбец account_number
 * считать intом, более подробно: {@link OrcMapper}
 */
public class Driver extends Configured implements Tool {
    private static String inputDir;
    private static String outputDir;
    private static final Logger LOG = LoggerFactory.getLogger(OrcMapper.class.getCanonicalName());

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            LOG.error("Invalid number of input arguments");
            LOG.warn("Input arguments: inputDir, outputDir");
            System.exit(-1);
        }
        inputDir = args[0];
        outputDir = args[1];
        ToolRunner.run(new Driver(), args);
    }

    @Override
    public int run(String[] args) throws Exception {

        Configuration conf = this.getConf();
        Job job = Job.getInstance(conf);
        job.setJarByClass(Driver.class);

        job.setMapperClass(OrcMapper.class);
        job.setNumReduceTasks(0);

        job.setInputFormatClass(TextInputFormat.class);
        FileInputFormat.addInputPath(job, new Path(inputDir));

        job.setOutputFormatClass(OrcNewOutputFormat.class);
        OrcNewOutputFormat.setOutputPath(job, new Path(outputDir));

        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(Writable.class);

        job.waitForCompletion(true);
        return 0;
    }

}

