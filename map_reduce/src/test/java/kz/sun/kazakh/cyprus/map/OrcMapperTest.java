package kz.sun.kazakh.cyprus.map;


import org.apache.hadoop.io.Text;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class OrcMapperTest  {
    Text validLine = new Text("1\tAddison\tMarks\t196296\tornare.lectus@et.edu");
    Text inValidLine = new Text("416\telvis\tRIVERA\t865-008\tQuisque.libero@vitaesodales.net");

    @Test
    public void parseLineTest(){

        List<Object> parsedFields = OrcMapper.parseLine(validLine);
        Assert.assertEquals(parsedFields.size(), 5);
    }

    /*
    Тестирую парсинг не валидной строки и корректность типизации полей.
     */
    @Test
    public void inValidLineParseLineTest(){

        List<Object> parsedFields = OrcMapper.parseLine(inValidLine);
        Assert.assertEquals(parsedFields.size(), 5);
        Assert.assertTrue(parsedFields.get(0) instanceof Integer);
        Assert.assertTrue(parsedFields.get(1) instanceof String);
        Assert.assertTrue(parsedFields.get(2) instanceof String);
        Assert.assertTrue(parsedFields.get(3) instanceof Integer);
        Assert.assertTrue(parsedFields.get(4) instanceof String);
    }
}
